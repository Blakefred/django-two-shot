from django.urls import path
from receipts.views import (
    create_receipt,
    accounts_list,
    category_list,
    receipt_list,
    create_category,
    create_account,
)

urlpatterns = [
    path('', receipt_list, name="home"),
    path('create/', create_receipt, name='create_receipt'),
    path('accounts/', accounts_list, name='account_list'),
    path('accounts/create/', create_account, name='create_account'),
    path('categories/', category_list, name='category_list'),
    path('categories/create/', create_category, name='create_category'),

]
